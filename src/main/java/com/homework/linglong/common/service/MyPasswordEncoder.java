package com.homework.linglong.common.service;

import org.springframework.security.crypto.password.PasswordEncoder;

public class MyPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence charSequence) {
        return charSequence.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return charSequence.toString().compareTo(s)==0;
    }

    @Override
    public boolean upgradeEncoding(String encodedPassword) {
        return false;
    }
}
