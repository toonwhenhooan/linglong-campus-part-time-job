package com.homework.linglong.common.service;

import com.homework.linglong.common.api.ResultCode;

public class ReturnCode {
    public static final int success = -ResultCode.SUCCESS.getCode();
    public static final int notFound = -ResultCode.NOTFOUND.getCode();
    public static final int forbidden = -ResultCode.FORBIDDEN.getCode();
    public static final int badRequest = - ResultCode.VALIDATE_FAILED.getCode();
    public static final int unKnownError = - ResultCode.FAILED.getCode();
}
