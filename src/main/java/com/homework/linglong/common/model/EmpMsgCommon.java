package com.homework.linglong.common.model;

public class EmpMsgCommon {
    public static enum  State{
        UnVerified(0), UnderUse(1), Finished(2);

        public final Byte value;
        private State(Integer val){
            this.value = val.byteValue();
        }

    }
}
