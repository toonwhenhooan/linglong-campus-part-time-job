package com.homework.linglong.common.api;

/**
 * 枚举了一些常用API操作码
 */

public enum ResultCode implements IErrorCode {
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(400, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或认证信息已经过期"),
    FORBIDDEN(403, "没有相关权限"),
    NOTFOUND(404, "找不到资源");
    private final int code;
    private final String message;

    ResultCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
