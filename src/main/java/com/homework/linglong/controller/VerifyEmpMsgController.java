package com.homework.linglong.controller;

import com.homework.linglong.common.api.CommonResult;
import com.homework.linglong.entity.mbg.EmpMsg;
import com.homework.linglong.entity.mbg.User;
import com.homework.linglong.service.EmpMsgService;
import com.homework.linglong.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api("待审核消息处理")
@RestController
@RequestMapping("/admin/msg/verify")
public class VerifyEmpMsgController {
    private EmpMsgService empMsgService;
    @Autowired
    public void setEmpMsgService(EmpMsgService empMsgService) {
        this.empMsgService = empMsgService;
    }

    @ApiOperation("获取还未审核的招聘信息")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public CommonResult<List<EmpMsg>> getUnVerifyMsg(){
        List<EmpMsg> empMsg = empMsgService.getUnVerifiedMsgList();
        return CommonResult.success(empMsg);
    }

    @ApiOperation(value = "修改招聘信息审核状态")
    @ApiParam(name = "msg_id", value = "招聘消息ID", example="1")
    @RequestMapping(value = "/{msg_id}", method = RequestMethod.PUT)
    public CommonResult<List<EmpMsg>> verifyMsg(@PathVariable(name = "msg_id") Integer msgId){
        EmpMsg empMsg = this.empMsgService.getMsgById(msgId);
        if(empMsg == null){
            CommonResult.notFound();
        }
        int affCount= empMsgService.verifyMsg(msgId);
        if (affCount>0) {
            return CommonResult.success(null, "修改成功");
        }else{
            return CommonResult.failed();
        }
    }

}
