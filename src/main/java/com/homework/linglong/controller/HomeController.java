package com.homework.linglong.controller;

import com.homework.linglong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
    UserService userService;

    @Autowired
    void setUserService(UserService service){
        this.userService = service;
    }

    @GetMapping("/")
    public String home(){
        return "index";
    }
}
