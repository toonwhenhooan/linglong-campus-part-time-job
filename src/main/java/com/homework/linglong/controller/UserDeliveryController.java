package com.homework.linglong.controller;

import com.homework.linglong.common.api.CommonResult;
import com.homework.linglong.service.DeliveryService;
import com.homework.linglong.service.EmpMsgService;
import com.homework.linglong.service.MultiTableQueryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/user/delivery")
public class UserDeliveryController {
    @Autowired
    DeliveryService deliveryService;
    @Autowired
    EmpMsgService empMsgService;
    @Autowired
    MultiTableQueryService multiTableQueryService;

    @ApiOperation(value = "申请工作")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public CommonResult applyJob(@RequestParam("msg_id") Integer msgId,@RequestBody(required = false) String neededInfo){
        int resultCode = deliveryService.askEmp(msgId, neededInfo);
        return CommonResult.resFromCode(resultCode);
    }

    @ApiOperation(value = "查看当前用户个人递交过的申请")
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public CommonResult lookDelivered(){
        Map data = multiTableQueryService.getCurrentUserDeliveryAndEmpMsg();
        return CommonResult.success(data);
    }
}
