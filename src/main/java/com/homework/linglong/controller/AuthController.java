package com.homework.linglong.controller;

import com.homework.linglong.common.annotation.NoSwaggerDoc;
import com.homework.linglong.common.api.CommonResult;
import com.homework.linglong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;

@Controller
@RequestMapping("/auth")
public class AuthController {

    @ResponseBody
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    CommonResult logIn(@RequestParam(value = "username") String username,
                       @RequestParam(value = "password") String password){
        return CommonResult.success(200);
        // todo: 可以考虑返回token 口令
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    String getLogIn(){
        return "/auth/login";
    }
}
