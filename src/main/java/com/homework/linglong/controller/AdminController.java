package com.homework.linglong.controller;

import com.homework.linglong.common.api.CommonResult;
import com.homework.linglong.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AdminService adminService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public CommonResult login(@RequestParam("username") String username, @RequestParam("password") String password){
        int result = adminService.logIn(username, password);
        return CommonResult.resFromCode(result);
    }
}
