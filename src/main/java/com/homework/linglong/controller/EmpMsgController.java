package com.homework.linglong.controller;

import com.homework.linglong.common.annotation.NoSwaggerDoc;
import com.homework.linglong.common.api.CommonResult;
import com.homework.linglong.entity.mbg.EmpMsg;
import com.homework.linglong.service.EmpMsgService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@NoSwaggerDoc
@RestController
@RequestMapping("")
public class EmpMsgController {
    @Autowired
    EmpMsgService empMsgService;

    @ApiOperation("获取用户创建的招聘信息列表")
    @RequestMapping(value = "/user/emp_msg", method = RequestMethod.GET)
    public CommonResult getUserEmpMsg(){
        List<EmpMsg> empMsgList = empMsgService.getUserMsgList();
        return CommonResult.success(empMsgList);
    }

    @ApiOperation("新建招聘信息")
    @RequestMapping(value = "/user/emp_msg", method = RequestMethod.POST)
    public CommonResult addEmpMsg(EmpMsg empMsg){
        int affCount = empMsgService.addEmpMsg(empMsg);
        if(affCount>0){
            return CommonResult.success(null);
        }else{
            return CommonResult.failed();
        }
    }

    @ApiOperation("修改招聘信息")
    @RequestMapping(value = "/user/emp_msg/{msgId}",method = RequestMethod.POST)
    public CommonResult updateEmpMsg(@PathVariable("msgId") Integer msgId, EmpMsg empMsg){
        int resultCode = empMsgService.updateUnverifiedEmpMsg(msgId, empMsg);
        return CommonResult.resFromCode(resultCode);
    }

    @ApiOperation("获取当前正在招聘的招聘信息")
    @RequestMapping(value = "/emp_msg", method = RequestMethod.GET)
    public CommonResult gainUnderwayEmpMsg(){
        List data = empMsgService.getUnderUseMsgList();
        return CommonResult.success(data);
    }
}
