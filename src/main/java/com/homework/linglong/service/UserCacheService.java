package com.homework.linglong.service;

import com.homework.linglong.entity.mbg.User;

/**
 * 用户信息缓存
 */
public interface UserCacheService {
    void setUser(User user);
    User getUser(Number userId);
    void delUser(Number userId);
    /**
     * 设置验证码
     */
    void setAuthCode(String telephone, String authCode);
    /**
     * 获取验证码
     */
    String getAuthCode(String telephone);
}
