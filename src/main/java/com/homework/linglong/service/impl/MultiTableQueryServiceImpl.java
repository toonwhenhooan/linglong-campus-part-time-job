package com.homework.linglong.service.impl;

import com.homework.linglong.dao.MultiTableQueryDao;
import com.homework.linglong.entity.mbg.User;
import com.homework.linglong.service.MultiTableQueryService;
import com.homework.linglong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class MultiTableQueryServiceImpl implements MultiTableQueryService {
    //@Autowired
    MultiTableQueryDao multiTableQueryDao;
    @Autowired
    UserService userService;

    @Override
    public HashMap getCurrentUserDeliveryAndEmpMsg() {
        User user = userService.getCurrentUser();
        return multiTableQueryDao.EmpMsgAndDeliveryByxUserId(user.getId());
    }
}
