package com.homework.linglong.service.impl;

import com.homework.linglong.common.service.ReturnCode;
import com.homework.linglong.dao.mbg.UserDao;
import com.homework.linglong.entity.AdminDetailsEntity;
import com.homework.linglong.entity.mbg.Admin;
import com.homework.linglong.entity.mbg.User;
import com.homework.linglong.entity.UserDetailsEntity;
import com.homework.linglong.entity.mbg.UserExample;
import com.homework.linglong.service.AdminService;
import com.homework.linglong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    final UserExample userExample = new UserExample();

    @Autowired
    private AdminService adminService;
    @Autowired
    private UserDao userDao;

    @Override
    public User selectByPrimaryKey(Integer id) {
        return userDao.selectByPrimaryKey(id);
    }

    @Override
    public User getCurrentUser() {
        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        UserDetailsEntity userDetailsEntity = (UserDetailsEntity) auth.getPrincipal();
        return userDetailsEntity.getCurrentUser();
    }

    @Override
    public Integer getCurrentUserId() {
        return getCurrentUser().getId();
    }

    @Override
    public User getByUsername(String username) {
        //此处应先查询缓存是否存在该用户
        userExample.clear();
        userExample.createCriteria().andPhoneEqualTo(username);
        List<User> users = this.userDao.selectByExample(userExample);
        User user = null;
        if (!CollectionUtils.isEmpty(users)){
            user = users.get(0);
            // 此处应添加用户缓存
        }
        return user;
    }

    @Override
    public int logIn(String username, String password) {
        return ReturnCode.success;
    }

    /**
     * 获取用户信息
     * @param username 用户名
     * @return User，不会是null
     * @throws UsernameNotFoundException 用户不存在
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        username = username.trim();
        if(hasLetters(username)){  // 动态决定登录的用户
            Admin admin = adminService.getByUsername(username);
            if(admin!=null){
                return new AdminDetailsEntity(admin);
            }
        }
        User user = getByUsername(username);
        if(user!=null){
            return new UserDetailsEntity(user);
        }
        throw new UsernameNotFoundException("用户名或密码错误");
    }

    /**
     * 判断字符串中是否有字母
     * @param str
     * @return
     */
    boolean hasLetters(String str){
        for (int i = str.length();--i>=0;){
            if (!Character.isDigit(str.charAt(i))){
                return true;
            }
        }
        return false;
    }
}
