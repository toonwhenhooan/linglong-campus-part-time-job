package com.homework.linglong.service.impl;

import com.homework.linglong.common.service.ReturnCode;
import com.homework.linglong.dao.mbg.DeliveryDao;
import com.homework.linglong.entity.mbg.Delivery;
import com.homework.linglong.entity.mbg.EmpMsg;
import com.homework.linglong.service.DeliveryService;
import com.homework.linglong.service.EmpMsgService;
import com.homework.linglong.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeliveryServiceImpl implements DeliveryService {
    @Autowired
    EmpMsgService empMsgService;
    @Autowired
    UserService userService;
    @Autowired
    DeliveryDao deliveryDao;

    @Override
    public int askEmp(Integer empMsgId, String neededInfo) {
        EmpMsg empMsg = empMsgService.getMsgById(empMsgId);
        if(empMsg == null){
            return ReturnCode.notFound;
        }
        String [] infoHeads = splitNeedInfo(empMsg.getInfoNeed());
        if (infoHeads != null) {
            String[] infoValues = splitNeedInfo(neededInfo);
            if (infoValues == null || infoHeads.length != infoValues.length) {
                return ReturnCode.badRequest;
            }
        }
        Delivery delivery = new Delivery(null, userService.getCurrentUserId(), empMsgId, neededInfo);
        deliveryDao.insertSelective(delivery);
        return ReturnCode.success;
    }

    private String[] splitNeedInfo(String info){
        if(info == null || info.isEmpty()) {
            return null;
        }
        return info.split("#");
    }

}
