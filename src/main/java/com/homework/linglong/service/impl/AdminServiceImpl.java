package com.homework.linglong.service.impl;

import com.homework.linglong.dao.mbg.AdminDao;
import com.homework.linglong.entity.AdminDetailsEntity;
import com.homework.linglong.entity.mbg.Admin;
import com.homework.linglong.entity.mbg.AdminExample;
import com.homework.linglong.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminDao adminDao;

    @Qualifier("userServiceImpl")
    @Autowired
    UserDetailsService userDetailsService;

    private final AdminExample adminExample;

    AdminServiceImpl(){
        adminExample = new AdminExample();
    }

    @Override
    public Admin getByAdminId(Integer id) {
        return null;
    }

    @Override
    public Admin getCurrentAdmin() {
        SecurityContext ctx = SecurityContextHolder.getContext();
        Authentication auth = ctx.getAuthentication();
        AdminDetailsEntity adminDetailsEntity = (AdminDetailsEntity) auth.getPrincipal();
        return adminDetailsEntity.getCurrentAdmin();
    }

    @Override
    public Admin getByUsername(String username) {
        //查缓存
        adminExample.clear();
        adminExample.createCriteria().andAccountEqualTo(username);
        var admins = adminDao.selectByExample(adminExample);
        return admins == null? null: admins.get(0);
    }

    @Override
    public int logIn(String username, String password) {
        return 0;
    }
}
