package com.homework.linglong.service;

import com.homework.linglong.entity.mbg.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User selectByPrimaryKey(Integer id);
    User getCurrentUser();
    Integer getCurrentUserId();
    User getByUsername(String username);
    int logIn(String username, String password);
}
