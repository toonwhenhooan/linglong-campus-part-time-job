package com.homework.linglong.service;

import com.homework.linglong.entity.mbg.EmpMsg;

import java.util.List;

public interface EmpMsgService {
    //获取未审核的招聘信息
    public List<EmpMsg> getUnVerifiedMsgList();
    public List<EmpMsg> getUnderUseMsgList();
    public List<EmpMsg> getUserMsgList();
    public int updateUnverifiedEmpMsg(Integer empMsgId, EmpMsg empMsg);
    public EmpMsg getMsgById(Integer msgId);
    public int verifyMsg(Integer msgId);
    public int addEmpMsg(EmpMsg empMsg);
}
