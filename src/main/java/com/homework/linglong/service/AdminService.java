package com.homework.linglong.service;

import com.homework.linglong.entity.mbg.Admin;

public interface AdminService {
    Admin getByAdminId(Integer id);
    Admin getCurrentAdmin();
    Admin getByUsername(String username);
    int logIn(String username, String password);
}
