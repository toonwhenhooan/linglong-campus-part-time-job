package com.homework.linglong.config;

import com.homework.linglong.common.annotation.NoSwaggerDoc;
import com.homework.linglong.config.property.SwaggerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootVersion;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.function.Predicate;

/**
 * Swagger API文档的配置
 */
@EnableOpenApi
@Configuration
public class SwaggerConfig{
    @Autowired
    private SwaggerProperties swaggerProperties;

    @Bean
    public Docket getDocket() {
        var docket = new Docket(DocumentationType.OAS_30);
        return docket
                .enable(swaggerProperties.getEnable())
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.homework.linglong.controller"))
                .apis(Predicate.not(RequestHandlerSelectors.withClassAnnotation(NoSwaggerDoc.class)))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title(swaggerProperties.getApplicationName() + " Api Doc")
                .title("玲珑校园兼职系统")
                .description(swaggerProperties.getApplicationDescription())
                .contact(new Contact("玲珑", null, "123456@qq.com"))
                .version("Application Version: " + swaggerProperties.getApplicationVersion() + ", Spring Boot Version: "
                        + SpringBootVersion.getVersion())
                .build();
    }
}
