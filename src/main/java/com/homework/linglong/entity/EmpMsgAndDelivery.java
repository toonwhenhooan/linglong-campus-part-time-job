package com.homework.linglong.entity;

import lombok.Data;

import java.util.Date;

@Data
public class EmpMsgAndDelivery {
    Integer deliveryId;
    Integer msgId;
    String msgTitle;
    String msgContent;
    Date deliveryTime;

}
