package com.homework.linglong.entity.mbg;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

public class EmpMsg implements Serializable {
    private Integer id;

    @ApiModelProperty(value = "发布者ID")
    private Integer userId;

    @ApiModelProperty(value = "发布内容")
    private String content;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "创建时间")
    private Date startTime;

    @ApiModelProperty(value = "招聘结束时间")
    private Date endTime;

    @ApiModelProperty(value = "招聘标题")
    private String title;

    @ApiModelProperty(value = "需要招聘人数")
    private Integer needNum;

    @ApiModelProperty(value = "招聘信息的状态，0：未审核1：正在进行招聘2：招聘结束")
    private Byte state;

    @ApiModelProperty(value = "需要填写的信息，每项信息之间用#隔开【type】_【length】_【title】")
    private String infoNeed;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getNeedNum() {
        return needNum;
    }

    public void setNeedNum(Integer needNum) {
        this.needNum = needNum;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public String getInfoNeed() {
        return infoNeed;
    }

    public void setInfoNeed(String infoNeed) {
        this.infoNeed = infoNeed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", content=").append(content);
        sb.append(", address=").append(address);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", title=").append(title);
        sb.append(", needNum=").append(needNum);
        sb.append(", state=").append(state);
        sb.append(", infoNeed=").append(infoNeed);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}