package com.homework.linglong.entity.mbg;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Delivery implements Serializable {
    private Integer id;

    @ApiModelProperty(value = "申请人id")
    private Integer userId;

    @ApiModelProperty(value = "招聘信息id")
    private Integer msgId;

    @ApiModelProperty(value = "递交的简历信息")
    private String infoDeliver;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMsgId() {
        return msgId;
    }

    public void setMsgId(Integer msgId) {
        this.msgId = msgId;
    }

    public String getInfoDeliver() {
        return infoDeliver;
    }

    public void setInfoDeliver(String infoDeliver) {
        this.infoDeliver = infoDeliver;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", userId=").append(userId);
        sb.append(", msgId=").append(msgId);
        sb.append(", infoDeliver=").append(infoDeliver);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}