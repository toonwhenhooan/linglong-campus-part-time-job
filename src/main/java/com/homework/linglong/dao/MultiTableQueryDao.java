package com.homework.linglong.dao;

import org.springframework.stereotype.Repository;

import java.util.HashMap;

public interface MultiTableQueryDao {
    HashMap<String, Object> EmpMsgAndDeliveryByDeliveryId(Integer deliveryId);
    HashMap<String, Object> EmpMsgAndDeliveryByxUserId(Integer userId);

}
