package com.homework.linglong.dao.mbg;

import com.homework.linglong.entity.mbg.Delivery;
import com.homework.linglong.entity.mbg.DeliveryExample;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DeliveryDao {
    long countByExample(DeliveryExample example);

    int deleteByExample(DeliveryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Delivery record);

    int insertSelective(Delivery record);

    List<Delivery> selectByExampleWithBLOBs(DeliveryExample example);

    List<Delivery> selectByExample(DeliveryExample example);

    Delivery selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Delivery record, @Param("example") DeliveryExample example);

    int updateByExampleWithBLOBs(@Param("record") Delivery record, @Param("example") DeliveryExample example);

    int updateByExample(@Param("record") Delivery record, @Param("example") DeliveryExample example);

    int updateByPrimaryKeySelective(Delivery record);

    int updateByPrimaryKeyWithBLOBs(Delivery record);

    int updateByPrimaryKey(Delivery record);
}