package com.homework.linglong.dao.mbg;

import com.homework.linglong.entity.mbg.EmpMsg;
import com.homework.linglong.entity.mbg.EmpMsgExample;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EmpMsgDao {
    long countByExample(EmpMsgExample example);

    int deleteByExample(EmpMsgExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EmpMsg record);

    int insertSelective(EmpMsg record);

    List<EmpMsg> selectByExample(EmpMsgExample example);

    EmpMsg selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EmpMsg record, @Param("example") EmpMsgExample example);

    int updateByExample(@Param("record") EmpMsg record, @Param("example") EmpMsgExample example);

    int updateByPrimaryKeySelective(EmpMsg record);

    int updateByPrimaryKey(EmpMsg record);
}