package com.homework.linglong;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
//@ComponentScan(basePackages = {"com.homework.linglong.service.impl", "com.homework.linglong.dao.*", "com.homework.linglong.config"})
@MapperScan("com.homework.linglong.dao.*")
public class LingLongApplication {
    public static void main(String[] args) {
        SpringApplication.run(LingLongApplication.class, args);
    }
}
